
;;; Code:


;;; * Document Article Macro
;; Default Values !!!
(defun tfm-setup-document-article()
  "Setup this project to export as an article document"
  (interactive)
  (if (eq (window-system) 'x)
  	  (org-toggle-inline-images t)
  	)
  (org-beamer-mode 0)
  (setq-local org-latex-packages-alist nil)
  (setq-local org-export-with-author t)
  (setq-local org-export-with-toc nil)
  (setq-local org-export-headline-levels 3)
  (setq-local org-export-exclude-tags '("noexport" "presentation"))
  (setq-local org-export-select-tags '("export" "document"))
  (setq-local org-use-tag-inheritance nil)
  (setq-local org-tags-exclude-from-inheritance '(annotated ATTACH))
  (setq-local org-latex-default-class "article")
  (setq-local org-latex-classes '( ("article" "\\documentclass[11pt]{article}"
									("\\section{%s}" . "\\section*{%s}")
									("\\subsection{%s}" . "\\subsection*{%s}")
									("\\subsubsection{%s}" . "\\subsubsection*{%s}")
									("\\paragraph{%s}" . "\\paragraph*{%s}")
									("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
								   )
			  )
  (setq-local org-latex-default-packages-alist '( ("latin9" "inputenc" t)
												  ("T1" "fontenc" t)
												  ("" "fixltx2e" nil)
												  ("" "graphicx" t)
												  ("" "longtable" nil)
												  ("" "float" nil)
												  ("" "wrapfig" nil)
												  ("normalem" "ulem" t)
												  ("" "textcomp" t)
												  ("" "marvosym" t)
												  ("" "wasysym" t)
												  ("" "latexsym" t)
												  ("" "amssymb" t)
												  ("" "amstext" nil)
												  ("bookmarks, pdftex, colorlinks=true, pdfstartview=FitV, linkcolor=blue, citecolor=blue, urlcolor=blue, citecolor=blue, urlcolor=blue, plainpages=false" "hyperref" nil)
												  "\\tolerance=1000"
												  ("" "eurosym" t)
												  ("" "makeidx" t)
												  ("" "lmodern" t)
												  ("" "pdfcolmk" t)
												  ("" "layout" t)
												  ("english" "babel" t)
												  ("top=3cm, bottom=3cm, left=3cm, right=3cm" "geometry" t)
												  ("" "url" t)
												  ("" "fancyhdr" t)
												  ("" "etoolbox" t)
												  ("" indentfirst t)
												  )
			  )
  ) ;; defun


;;; * Presentation Beamer Macro
;; Default Values !!!
(defun tfm-setup-presentation-beamer()
"Setup this project to export as a beamer presentation."
  (interactive)
  (if (eq (window-system) 'x)
  	  (org-toggle-inline-images t)
  	)
  (org-beamer-mode 1)
  (setq-local org-latex-packages-alist nil)
  (setq-local org-export-with-author t)
  (setq-local org-export-with-toc t)
  (setq-local org-beamer-frame-level 2)
  ;; (setq-local org-export-headline-levels 4)
  (setq-local org-export-select-tags '("export" "presentation"))
  (setq-local org-export-exclude-tags '("noexport" "document"))
  (setq-local org-use-tag-inheritance t)
  (setq-local org-tags-exclude-from-inheritance '(annotated ATTACH))
  ;; sets latex class
  (setq-local org-latex-default-class "beamer")
  ;; #+LaTeX_CLASS_OPTIONS: [table,smaller, 12pt, handout, presentation, bigger]
  (setq-local org-latex-classes
			  '(
				("beamer" "\\documentclass[presentation, 10pt]{beamer}\n[DEFAULT-PACKAGES]\n[PACKAGES]\n[EXTRA]"
				 ("\\section{%s}" . "\\section*{%s}")
				 ("\\subsection{%s}" . "\\subsection*{%s}")
				 ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))
				)
			  )
  (setq-local org-latex-default-packages-alist '(
												 ("AUTO" "inputenc" t)
												 ("T1" "fontenc" t)
												 ("" "fixltx2e" nil)
												 ("" "graphicx" t)
												 ("" "longtable" nil)
												 ("" "float" nil)
												 ("" "wrapfig" nil)
												 ("normalem" "ulem" t)
												 ("" "textcomp" t)
												 ("" "marvosym" t)
												 ("" "wasysym" t)
												 ("" "latexsym" t)
												 ("" "amssymb" t)
												 ("" "amstext" nil)
												 ("" "hyperref" nil)
												 ("" "etoolbox" t)
												 "\\tolerance=1000")
			  )
  (add-to-list 'org-beamer-environments-default '("center" "J" "\\begin{center}%a{%h}" "\\end{center}"))
  ) ;; defun





;; (setq org-beamer-environments-default '(("block" "b" "\\begin{block}%a{%h}" "\\end{block}")
;;  ("alertblock" "a" "\\begin{alertblock}%a{%h}" "\\end{alertblock}")
;;  ("verse" "v" "\\begin{verse}%a %% %h" "\\end{verse}")
;;  ("quotation" "q" "\\begin{quotation}%a %% %h" "\\end{quotation}")
;;  ("quote" "Q" "\\begin{quote}%a %% %h" "\\end{quote}")
;;  ("structureenv" "s" "\\begin{structureenv}%a %% %h" "\\end{structureenv}")
;;  ("theorem" "t" "\\begin{theorem}%a%U" "\\end{theorem}")
;;  ("definition" "d" "\\begin{definition}%a%U" "\\end{definition}")
;;  ("example" "e" "\\begin{example}%a%U" "\\end{example}")
;;  ("exampleblock" "E" "\\begin{exampleblock}%a{%h}" "\\end{exampleblock}")
;;  ("proof" "p" "\\begin{proof}%a%U" "\\end{proof}")
;;  ("beamercolorbox" "o" "\\begin{beamercolorbox}%o{%h}" "\\end{beamercolorbox}"))
;; )


(provide 'tfm_presentacion)

;;; tfm_presentacion.el ends here
